<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Repository\ShiptoRepository;

/**
 * @ORM\Entity(repositoryClass=ShiptoRepository::class)
 */
class Shipto
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $address;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $city;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $country;

    /**
     * @ORM\OneToOne(targetEntity=Shiporder::class, mappedBy="shipto", cascade={"persist", "remove"})
     */
    private $shiporder;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string|null $name
     * @return $this
     */
    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getAddress(): ?string
    {
        return $this->address;
    }

    /**
     * @param string|null $address
     * @return $this
     */
    public function setAddress(?string $address): self
    {
        $this->address = $address;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getCity(): ?string
    {
        return $this->city;
    }

    /**
     * @param string|null $city
     * @return $this
     */
    public function setCity(?string $city): self
    {
        $this->city = $city;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getCountry(): ?string
    {
        return $this->country;
    }

    /**
     * @param string|null $country
     * @return $this
     */
    public function setCountry(?string $country): self
    {
        $this->country = $country;

        return $this;
    }

    /**
     * @return Shiporder|null
     */
    public function getShiporder(): ?Shiporder
    {
        return $this->shiporder;
    }

    /**
     * @param Shiporder|null $shiporder
     * @return $this
     */
    public function setShiporder(?Shiporder $shiporder): self
    {
        if ($shiporder === null && $this->shiporder !== null) {
            $this->shiporder->setShipto(null);
        }

        if ($shiporder !== null && $shiporder->getShipto() !== $this) {
            $shiporder->setShipto($this);
        }

        $this->shiporder = $shiporder;

        return $this;
    }
}
