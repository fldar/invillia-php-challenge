<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Repository\PeopleRepository;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=PeopleRepository::class)
 */
class People
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"list_people"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $name;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Groups({"list_people"})
     */
    private $integrationId;

    /**
     * @ORM\OneToMany(targetEntity=Shiporder::class, mappedBy="people")
     * @Groups({"list_people"})
     */
    private $shiporders;

    /**
     * @ORM\OneToMany(targetEntity=PeoplePhones::class, mappedBy="people")
     * @Groups({"list_people"})
     */
    private $phones;

    public function __construct()
    {
        $this->phones = new ArrayCollection();
        $this->shiporders = new ArrayCollection();
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return $this
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|PeoplePhones[]
     */
    public function getPhones(): Collection
    {
        return $this->phones->map(function ($phone) {
            return $phone->getNumber();
        });
    }

    /**
     * @param PeoplePhones $peoplePhone
     * @return $this
     */
    public function addPhone(PeoplePhones $peoplePhone): self
    {
        if (!$this->phones->contains($peoplePhone)) {
            $this->phones[] = $peoplePhone;
            $peoplePhone->setPeople($this);
        }

        return $this;
    }

    /**
     * @param PeoplePhones $peoplePhone
     * @return $this
     */
    public function removePhone(PeoplePhones $peoplePhone): self
    {
        if ($this->phones->removeElement($peoplePhone)) {
            if ($peoplePhone->getPeople() === $this) {
                $peoplePhone->setPeople(null);
            }
        }

        return $this;
    }

    /**
     * @return int|null
     */
    public function getIntegrationId(): ?int
    {
        return $this->integrationId;
    }

    /**
     * @param int|null $integrationId
     * @return $this
     */
    public function setIntegrationId(?int $integrationId): self
    {
        $this->integrationId = $integrationId;

        return $this;
    }

    /**
     * @return Collection|Shiporder[]
     */
    public function getShiporders(): Collection
    {
        return $this->shiporders->map(function ($shiporder) {
            return $shiporder->getId();
        });
    }

    /**
     * @param Shiporder $shiporder
     * @return $this
     */
    public function addShiporder(Shiporder $shiporder): self
    {
        if (!$this->shiporders->contains($shiporder)) {
            $this->shiporders[] = $shiporder;
            $shiporder->setPeople($this);
        }

        return $this;
    }

    /**
     * @param Shiporder $shiporder
     * @return $this
     */
    public function removeShiporder(Shiporder $shiporder): self
    {
        if ($this->shiporders->removeElement($shiporder)) {
            if ($shiporder->getPeopleId() === $this) {
                $shiporder->setPeopleId(null);
            }
        }

        return $this;
    }
}
