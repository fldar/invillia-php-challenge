<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Repository\ShiporderRepository;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity(repositoryClass=ShiporderRepository::class)
 */
class Shiporder
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=People::class, inversedBy="shiporders")
     */
    private $people;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $integrationId;

    /**
     * @ORM\OneToOne(targetEntity=Shipto::class, inversedBy="shiporder", cascade={"persist", "remove"})
     */
    private $shipto;

    /**
     * @ORM\OneToMany(targetEntity=ShiporderItems::class, mappedBy="shiporder")
     */
    private $shiporderItems;

    public function __construct()
    {
        $this->shiporderItems = new ArrayCollection();
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getPeople(): ?string
    {
        return $this->people->getName();
    }

    /**
     * @param People|null $peopleId
     * @return $this
     */
    public function setPeople(?People $peopleId): self
    {
        $this->people = $peopleId;

        return $this;
    }

    /**
     * @return array
     */
    public function getShipto(): array
    {
        if (empty($this->shipto)) {
            return [];
        }

        return [
            'address' => $this->shipto->getAddress(),
            'city' => $this->shipto->getCity(),
            'country' => $this->shipto->getCountry(),
        ];
    }

    /**
     * @param Shipto $shipto
     * @return $this
     */
    public function setShipto(Shipto $shipto): self
    {
        $this->shipto = $shipto;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getIntegrationId(): ?int
    {
        return $this->integrationId;
    }

    /**
     * @param int|null $integrationId
     * @return $this
     */
    public function setIntegrationId(?int $integrationId): self
    {
        $this->integrationId = $integrationId;

        return $this;
    }

    /**
     * @return Collection|ShiporderItems[]
     */
    public function getShiporderItems(): Collection
    {
        return $this->shiporderItems->map(function ($item) {
            return [
                'title' => $item->getTitle(),
                'note' => $item->getNote(),
                'quantity' => $item->getQuantity(),
                'price' => $item->getPrice()
            ];
        });
    }

    /**
     * @param ShiporderItems $shiporderItem
     * @return $this
     */
    public function addShiporderItem(ShiporderItems $shiporderItem): self
    {
        if (!$this->shiporderItems->contains($shiporderItem)) {
            $this->shiporderItems[] = $shiporderItem;
            $shiporderItem->setShiporder($this);
        }

        return $this;
    }

    /**
     * @param ShiporderItems $shiporderItem
     * @return $this
     */
    public function removeShiporderItem(ShiporderItems $shiporderItem): self
    {
        if ($this->shiporderItems->removeElement($shiporderItem)) {
            if ($shiporderItem->getShiporder() === $this) {
                $shiporderItem->setShiporder(null);
            }
        }

        return $this;
    }
}
