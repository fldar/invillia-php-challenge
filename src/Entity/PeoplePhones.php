<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Repository\PeoplePhonesRepository;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=PeoplePhonesRepository::class)
 */
class PeoplePhones
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=11)
     */
    private $number;

    /**
     * @ORM\ManyToOne(targetEntity=People::class, inversedBy="phones", cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $people;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getNumber(): ?string
    {
        return $this->number;
    }

    /**
     * @param string $numer
     * @return $this
     */
    public function setNumber(string $numer): self
    {
        $this->number = $numer;

        return $this;
    }

    /**
     * @return People|null
     */
    public function getPeople(): ?People
    {
        return $this->people;
    }

    /**
     * @param People|null $people
     * @return $this
     */
    public function setPeople(?People $people): self
    {
        $this->people = $people;

        return $this;
    }
}
