<?php

namespace App\Controller\Front;

use App\Form\XmlImportType;
use App\Service\XmlImportService;
use Symfony\Component\Routing\Annotation\Route;
use Laminas\EventManager\Exception\DomainException;
use Symfony\Component\HttpFoundation\{Response, Request};
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/web/xml")
 * @package App\Controller\Front
 */
class XmlImportController extends AbstractController
{
    /** @var string  */
    public const FRONT = 'front/xml_import/index.html.twig';

    private XmlImportService $service;

    /**
     * @param XmlImportService $service
     */
    public function __construct(XmlImportService $service)
    {
        $this->service = $service;
    }

    /**
     * @Route("/index", name="xml_index")
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request): Response
    {
        $form = $this->createForm(XmlImportType::class)->handleRequest($request);

        try {
            $result = $this->service->process($form);
        } catch (DomainException $domainException) {
            return $this->render(self::FRONT, [
                'form' => $form->createView(),
                'error' => $domainException->getMessage()
            ]);
        }

        return $this->render(self::FRONT, ['form' => $form->createView(), 'success' => $result]);
    }
}
