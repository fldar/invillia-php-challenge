<?php

namespace App\Controller\Api;

use App\Service\ShiporderService;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/api/shiporder")
 * @package App\Controller\Api
 */
class ShiporderController extends AbstractController
{
    private ShiporderService $service;

    /**
     * @param ShiporderService $service
     */
    public function __construct(ShiporderService $service)
    {
        $this->service = $service;
    }

    /**
     * @Route("/", name="api_shiporder", methods={"GET"})
     */
    public function all(): Response
    {
        return $this->json($this->service->all());
    }
}
