<?php

namespace App\Controller\Api;

use App\Service\PeopleService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * @Route("/api/people", name="api_people")
 * @package App\Controller\Api
 */
class PeopleController extends AbstractController
{
    private PeopleService $service;

    /**
     * @param PeopleService $service
     */
    public function __construct(PeopleService $service)
    {
        $this->service = $service;
    }

    /**
     * @Route("/", name="api_people", methods={"GET"})
     */
    public function all(): Response
    {
        return $this->json($this->service->all());
    }
}
