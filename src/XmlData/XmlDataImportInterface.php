<?php

namespace App\XmlData;

use Doctrine\ORM\Mapping\Entity;
use App\Repository\XmlDataRepository;

/**
 * @package App\XmlData
 */
interface XmlDataImportInterface
{
    /**
     * @param array $data
     * @param XmlDataRepository $repository
     */
    public function __construct(array $data, XmlDataRepository $repository);

    public function import(): void;

    public function setRepository(XmlDataRepository $repository): void;

    public function insertData(array $data);
}
