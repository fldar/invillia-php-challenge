<?php

namespace App\XmlData;

use App\Entity\People;
use App\Entity\PeoplePhones;

class PeopleXmlDataImport extends XmlDataImportAbstract
{
    /**
     * @param array $people
     */
    public function insertData(array $people): void
    {
        try {
            $peopleEntity = $this->getPeopleOrNew($people['personid'] ?? null);

            if (!$peopleEntity->getId()) {
                $peopleEntity->setName($people['personname'] ?? '');
                $peopleEntity->setIntegrationId($people['personid'] ?? '');
            }

            $peopleEntity = $this->addPhones($peopleEntity, $people['phones'] ?? null);

            $this->repository->insertFromXml($peopleEntity);
        } catch (\Throwable $throwable) {
        }
    }

    /**
     * @param People $peopleEntity
     * @param array|null $phones
     * @return People
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function addPhones(People $peopleEntity, ?array $phones = null): People
    {
        if (empty($phones)) {
            return $peopleEntity;
        }

        if (is_array($phones['phone'])) {
            $phones = $phones['phone'];
        }

        foreach ($phones as $phone) {
            $phoneEnitty = new PeoplePhones();
            $phoneEnitty->setNumber($phone);
            $phoneEnitty->setPeople($peopleEntity);

            $this->repository->insertFromXml($phoneEnitty);
        }

        return $peopleEntity;
    }

    /**
     * @param int|null $integrationId
     * @return People
     */
    private function getPeopleOrNew(?int $integrationId): People
    {
        $people = $this->repository->findByIntegrationId($integrationId);

        return $people ?: new People();
    }
}
