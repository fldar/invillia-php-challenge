<?php

namespace App\XmlData;

use App\Entity\People;
use App\Entity\Shiporder;
use App\Repository\ShiporderRepository;
use Doctrine\ORM\EntityManagerInterface;
use Laminas\EventManager\Exception\DomainException;

class XmlDataFactory
{
    /** @var string  */
    public const NOT_IMPLEMENTED = 'This type of file has not been implemented';

    /** @var string  */
    private const
        PEOPLE    = 'person',
        SHIPORDER = 'shiporder'
    ;

    /**
     * @param \SimpleXMLElement $xmlData
     * @param EntityManagerInterface $entityManager
     * @return PeopleXmlDataImport|ShipOrderXmlDataImport
     */
    public static function create(\SimpleXMLElement $xmlData, EntityManagerInterface $entityManager)
    {
        $data = json_decode(json_encode($xmlData), true);

        switch (self::getDataKey($data)) {
            case self::PEOPLE:
                return new PeopleXmlDataImport($data['person'], $entityManager->getRepository(People::class));
            case self::SHIPORDER:
                $shipOrderXmlDataImport = new ShipOrderXmlDataImport(
                    $data['shiporder'],
                    $entityManager->getRepository(Shiporder::class)
                );
                $shipOrderXmlDataImport->setPeopleRepository($entityManager->getRepository(People::class));

                return $shipOrderXmlDataImport;
            default:
                throw new DomainException(self::NOT_IMPLEMENTED);
        }
    }

    /**
     * @param array $data
     * @return int|string
     */
    public static function getDataKey(array $data): string
    {
        return array_keys($data)[0] ?? '';
    }
}
