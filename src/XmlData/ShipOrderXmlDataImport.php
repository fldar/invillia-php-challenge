<?php

namespace App\XmlData;

use App\Entity\People;
use App\Entity\PeoplePhones;
use App\Entity\Shiporder;
use App\Entity\ShiporderItems;
use App\Entity\Shipto;
use App\Repository\PeopleRepository;

class ShipOrderXmlDataImport extends XmlDataImportAbstract
{
    protected PeopleRepository $peopleRepository;

    /**
     * @param array $shiporder
     */
    public function insertData(array $shiporder): void
    {
        try {
            $people = $this->peopleRepository->findByIntegrationId($shiporder['orderperson'] ?? null);

            if (empty($people)) {
                return;
            }

            $shiporderEntity = new Shiporder();
            $shiporderEntity->setPeople($people);
            $shiporderEntity->setIntegrationId($shiporder['orderid'] ?? null);
            $this->addShipto($shiporderEntity, $shiporder['shipto'] ?? null);

            $shiporderEntity = $this->addItems($shiporderEntity, $shiporder['items'] ?? null);
            $this->repository->insertFromXml($shiporderEntity);
        } catch (\Throwable $throwable) {
            dd($throwable);
        }
    }

    /**
     * @param Shiporder $shiporderEntity
     * @param array|null $shipto
     * @return Shiporder
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function addShipto(Shiporder $shiporderEntity, ?array $shipto)
    {
        if (empty($shipto)) {
            return $shiporderEntity;
        }

        $shiptoEntity = new Shipto();
        $shiptoEntity->setName($shipto['name']);
        $shiptoEntity->setAddress($shipto['address']);
        $shiptoEntity->setCity($shipto['city']);
        $shiptoEntity->setCountry($shipto['country']);
        $shiptoEntity->setCountry($shipto['country']);
        $shiptoEntity->setShiporder($shiporderEntity);

        $this->repository->insertFromXml($shiporderEntity);
    }

    /**
     * @param Shiporder $shiporderEntity
     * @param array|null $items
     * @return Shiporder
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function addItems(Shiporder $shiporderEntity, ?array $items = null): Shiporder
    {
        if (empty($items)) {
            return $shiporderEntity;
        }

        if (is_array($items) && !isset($items['item']['price'])) {
            $items = $items['item'];
        }

        foreach ($items as $item) {
            $shiporderItem = new ShiporderItems();
            $shiporderItem->setShiporder($shiporderEntity);
            $shiporderItem->setNote($item['note'] ?? '');
            $shiporderItem->setTitle($item['title'] ?? '');
            $shiporderItem->setQuantity($item['quantity'] ?? 0);
            $shiporderItem->setPrice($item['price'] ?? 0);

            $this->repository->insertFromXml($shiporderItem);
        }

        return $shiporderEntity;
    }

    /**
     * @param PeopleRepository $peopleRepository
     */
    public function setPeopleRepository(PeopleRepository $peopleRepository): void
    {
        $this->peopleRepository = $peopleRepository;
    }
}
