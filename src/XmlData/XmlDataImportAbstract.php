<?php

namespace App\XmlData;

use App\Repository\XmlDataRepository;

/**
 * @package App\XmlData
 */
abstract class XmlDataImportAbstract implements XmlDataImportInterface
{
    protected array $data;
    protected XmlDataRepository $repository;

    /**
     * @param array $data
     * @param XmlDataRepository $repository
     */
    public function __construct(array $data, XmlDataRepository $repository)
    {
        $this->data = $data;

        $this->setRepository($repository);
    }

    public function import(): void
    {
        foreach ($this->data as $data) {
            $this->insertData($data);
        }
    }

    /**
     * @param array $data
     */
    abstract public function insertData(array $data): void;

    /**
     * @param XmlDataRepository $repository
     */
    public function setRepository(XmlDataRepository $repository): void
    {
        $this->repository = $repository;
    }
}
