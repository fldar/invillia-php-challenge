<?php

namespace App\Form;

use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\Form\{AbstractType, FormBuilderInterface};
use Symfony\Component\Form\Extension\Core\Type\{FileType, SubmitType};

class XmlImportType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('file', FileType::class, [
                'label' => 'XML',
                'required' => true,
                'constraints' => [
                    new File([
                        'mimeTypes' => [
                            'text/xml',
                        ]
                    ])
                ]
            ])
            ->add('submit', SubmitType::class, ['label' => 'Enviar'])
        ;
    }
}
