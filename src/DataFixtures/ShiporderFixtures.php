<?php

namespace App\DataFixtures;

use App\Entity\Shipto;
use App\Entity\People;
use App\Entity\Shiporder;
use App\Entity\ShiporderItems;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class ShiporderFixtures extends Fixture
{
    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $people = $manager->getRepository(People::class)->findByIntegrationId(100);
        $shiporder = new Shiporder();
        $shipto = new Shipto();
        $shiporderItens = new ShiporderItems();

        $shipto->setName('Bla');
        $shipto->setCountry('Bla');
        $shipto->setCity('Bla');
        $shipto->setAddress('Bla');

        $shiporderItens->setPrice(10);
        $shiporderItens->setQuantity(1);
        $shiporderItens->setTitle('Bla');
        $shiporderItens->setNote('Bla');
        $shiporderItens->setShiporder($shiporder);

        $shiporder->setPeople($people);
        $shiporder->setShipto($shipto);

        $manager->persist($shiporderItens);
        $manager->persist($shiporder);
        $manager->flush();
    }
}
