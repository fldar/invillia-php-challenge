<?php

namespace App\DataFixtures;

use App\Entity\People;
use App\Entity\PeoplePhones;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class PeopleFixtures extends Fixture
{
    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager): void
    {
        $people = new People();
        $phone = new PeoplePhones();

        $people->setName('Bla');
        $people->setIntegrationId('100');

        $phone->setNumber('123456');
        $phone->setPeople($people);

        $manager->persist($phone);
        $manager->flush();
    }
}
