<?php

namespace App\Repository;

use App\Entity\PeoplePhones;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

/**
 * @method PeoplePhones|null find($id, $lockMode = null, $lockVersion = null)
 * @method PeoplePhones|null findOneBy(array $criteria, array $orderBy = null)
 * @method PeoplePhones[]    findAll()
 * @method PeoplePhones[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PeoplePhonesRepository extends ServiceEntityRepository
{
    /**
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PeoplePhones::class);
    }
}
