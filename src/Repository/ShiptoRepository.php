<?php

namespace App\Repository;

use App\Entity\Shipto;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

/**
 * @method Shipto|null find($id, $lockMode = null, $lockVersion = null)
 * @method Shipto|null findOneBy(array $criteria, array $orderBy = null)
 * @method Shipto[]    findAll()
 * @method Shipto[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ShiptoRepository extends ServiceEntityRepository
{
    /**
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Shipto::class);
    }
}
