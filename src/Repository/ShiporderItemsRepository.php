<?php

namespace App\Repository;

use App\Entity\ShiporderItems;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

/**
 * @method ShiporderItems|null find($id, $lockMode = null, $lockVersion = null)
 * @method ShiporderItems|null findOneBy(array $criteria, array $orderBy = null)
 * @method ShiporderItems[]    findAll()
 * @method ShiporderItems[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ShiporderItemsRepository extends ServiceEntityRepository
{
    /**
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ShiporderItems::class);
    }
}
