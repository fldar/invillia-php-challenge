<?php

namespace App\Repository;

use App\Entity\People;
use App\Entity\PeoplePhones;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method People|null find($id, $lockMode = null, $lockVersion = null)
 * @method People|null findOneBy(array $criteria, array $orderBy = null)
 * @method People[]    findAll()
 * @method People[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PeopleRepository extends XmlDataRepository
{
    /**
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, People::class);
    }

    /**
     * @param int|null $integrationId
     * @return People|null
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findByIntegrationId(?int $integrationId): ?People
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.integrationId = :integrationId')
            ->setParameter('integrationId', $integrationId)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
}
