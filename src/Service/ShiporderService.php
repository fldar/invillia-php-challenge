<?php

namespace App\Service;

use App\Repository\ShiporderRepository;
use Doctrine\Common\Collections\ArrayCollection;

class ShiporderService
{
    private ShiporderRepository $repository;

    /**
     * @param ShiporderRepository $repository
     */
    public function __construct(ShiporderRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @return array
     */
    public function all(): array
    {
        return $this->repository->findAll();
    }
}
