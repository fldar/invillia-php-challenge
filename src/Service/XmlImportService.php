<?php

namespace App\Service;

use App\XmlData\XmlDataFactory;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\{Form, FormInterface};
use Laminas\EventManager\Exception\DomainException;

/**
 * @package App\Service
 */
class XmlImportService
{
    /** @var string  */
    public const
        ERROR = 'Please upload a valid XML file.',
        SUCCESS = 'Import performed successfully.'
    ;

    private EntityManagerInterface $entityManager;

    /**
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param FormInterface $form
     * @return string|null
     */
    public function process(FormInterface $form): ?string
    {
        if (!$form->isSubmitted()) {
            return null;
        }

        $this->validPost($form);

        XmlDataFactory::create($this->getXmlData($form), $this->entityManager)
            ->import()
        ;

        return self::SUCCESS;
    }

    /**
     * @param Form $form
     */
    private function validPost(FormInterface $form): void
    {
        if (!$form->isValid()) {
            throw new DomainException(self::ERROR);
        }
    }

    /**
     * @param Form $form
     * @return \SimpleXMLElement
     */
    private function getXmlData(Form $form): \SimpleXMLElement
    {
        $file = $form->get('file')->getData();

        return simplexml_load_file($file);
    }
}
