<?php

namespace App\Service;

use App\Repository\PeopleRepository;
use Doctrine\Common\Collections\ArrayCollection;

class PeopleService
{
    private PeopleRepository $repository;

    /**
     * @param PeopleRepository $repository
     */
    public function __construct(PeopleRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @return array
     */
    public function all(): array
    {
        return $this->repository->findAll();
    }
}
