if [ $1 -eq 1 ]; then
  docker-compose up -d
  docker-compose exec -T php composer install
  docker-compose exec -T php bin/console doctrine:database:drop --no-interaction --force --if-exists
  docker-compose exec -T php bin/console doctrine:database:create --no-interaction
  docker-compose exec -T php bin/console doctrine:migrations:migrate --no-interaction
  docker-compose exec -T php bin/console doctrine:fixtures:load --no-interaction
  exit
fi

if [ $1 -eq 2 ]; then
  docker-compose exec -T php ./vendor/bin/phpcs src/ --standard=PSR12 -p --ignore=*/Migrations/* --ignore=*Kernel.php*
  exit 
fi

if [ $1 -eq 3 ]; then
  docker-compose exec -T php bin/console doctrine:database:drop --no-interaction --force --if-exists --env=test
  docker-compose exec -T php bin/console doctrine:database:create --no-interaction --env=test
  docker-compose exec -T php bin/console doctrine:migrations:migrate --no-interaction --env=test
  docker-compose exec -T php bin/console doctrine:fixtures:load --no-interaction --env=test
  docker-compose exec -T php vendor/bin/simple-phpunit --coverage-text
  exit;
else
  echo "Opção inválida"
fi
