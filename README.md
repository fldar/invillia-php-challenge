## INVILLIA PHP CHALLENGE
##### Requisitos
- docker-compose version 1.27.4, build 40524192
- Docker version 20.10.3, build 48d30b5

##### Preparação
- Crie uma pasta chamada `deve` na raiz `sudo mkdir -m 777 /deve`
- Abra a pasta `cd /deve`
- Clone o projeto para ela `git clone https://gitlab.com/fldar/invillia-php-challenge.git`
- Abra o projeto `cd invillia-php-challenge`
- Execute: `docker-compose build`

#### OBS
- Só necessário SEGUIR um dos métodos "AUTOMATIZADO" ou "MANUAL"

##### Instalação [AUTOMATIZADO]
- Execute: `sudo chmod +x dev.sh` OBS: Necessário executar apenas uma vez.
- Execute: `./dev.sh 1`
- Sistema estará disponivel em: 'http://localhost:81/web/xml/index'

##### Instalação [MANUAL]
- Execute: `docker-compose up -d`
- Execute: `docker-compose exec -T php composer install`
- Execute: `docker-compose exec -T php bin/console doctrine:database:drop --no-interaction --force --if-exists`
- Execute: `docker-compose exec -T php bin/console doctrine:database:create --no-interaction`
- Execute: `docker-compose exec -T php bin/console doctrine:migrations:migrate --no-interaction`
- Execute: `docker-compose exec -T php bin/console doctrine:fixtures:load --no-interaction`
- Sistema estará disponivel em: 'http://localhost:81/web/xml/index'


#### Code Sniffer (Verificar PSR) [AUTOMATIZADO]
- Execute: `./dev.sh 2`

#### Code Sniffer (Verificar PSR) [MANUAL]
- Execute: `docker-compose exec -T php ./vendor/bin/phpcs src/ --standard=PSR12 -p --ignore=*/Migrations/* --ignore=*Kernel.php*`


#### Testes Unitários [AUTOMATIZADO]
- Execute: `./dev.sh 3`

#### Testes Unitários [MANUAL]
- Execute: `docker-compose exec -T php bin/console doctrine:database:drop --no-interaction --force --if-exists --env=test`
- Execute: `docker-compose exec -T php bin/console doctrine:database:create --no-interaction --env=test`
- Execute: `docker-compose exec -T php bin/console doctrine:migrations:migrate --no-interaction --env=test`
- Execute: `docker-compose exec -T php bin/console doctrine:fixtures:load --no-interaction --env=test`
- Execute: `docker-compose exec -T php vendor/bin/simple-phpunit --coverage-text`


## API
## Recuperar dados de Pessoas
- GET: `localhost:81/api/people`

## Recuperar dados de Ordem de Entrega
- GET: `localhost:81/api/shiporder`
