<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210526144415 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE people (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(100) NOT NULL, integration_id INT DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE people_phones (id INT AUTO_INCREMENT NOT NULL, people_id INT NOT NULL, number VARCHAR(11) NOT NULL, INDEX IDX_953F0C1F3147C936 (people_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE shiporder (id INT AUTO_INCREMENT NOT NULL, people_id INT DEFAULT NULL, shipto_id INT DEFAULT NULL, integration_id INT DEFAULT NULL, INDEX IDX_A2E30C23147C936 (people_id), UNIQUE INDEX UNIQ_A2E30C2CF32E93F (shipto_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE shiporder_items (id INT AUTO_INCREMENT NOT NULL, shiporder_id INT NOT NULL, title VARCHAR(50) NOT NULL, note VARCHAR(50) NOT NULL, quantity DOUBLE PRECISION NOT NULL, price DOUBLE PRECISION NOT NULL, INDEX IDX_DB5D5F33F7FABC28 (shiporder_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE shipto (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(100) DEFAULT NULL, address VARCHAR(100) DEFAULT NULL, city VARCHAR(50) DEFAULT NULL, country VARCHAR(50) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE people_phones ADD CONSTRAINT FK_953F0C1F3147C936 FOREIGN KEY (people_id) REFERENCES people (id)');
        $this->addSql('ALTER TABLE shiporder ADD CONSTRAINT FK_A2E30C23147C936 FOREIGN KEY (people_id) REFERENCES people (id)');
        $this->addSql('ALTER TABLE shiporder ADD CONSTRAINT FK_A2E30C2CF32E93F FOREIGN KEY (shipto_id) REFERENCES shipto (id)');
        $this->addSql('ALTER TABLE shiporder_items ADD CONSTRAINT FK_DB5D5F33F7FABC28 FOREIGN KEY (shiporder_id) REFERENCES shiporder (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE people_phones DROP FOREIGN KEY FK_953F0C1F3147C936');
        $this->addSql('ALTER TABLE shiporder DROP FOREIGN KEY FK_A2E30C23147C936');
        $this->addSql('ALTER TABLE shiporder_items DROP FOREIGN KEY FK_DB5D5F33F7FABC28');
        $this->addSql('ALTER TABLE shiporder DROP FOREIGN KEY FK_A2E30C2CF32E93F');
        $this->addSql('DROP TABLE people');
        $this->addSql('DROP TABLE people_phones');
        $this->addSql('DROP TABLE shiporder');
        $this->addSql('DROP TABLE shiporder_items');
        $this->addSql('DROP TABLE shipto');
    }
}
