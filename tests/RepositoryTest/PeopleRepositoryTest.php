<?php

namespace App\Tests\RepositoryTest;

use App\Entity\People;
use Doctrine\ORM\EntityManager;
use App\Repository\PeopleRepository;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class PeopleRepositoryTest extends KernelTestCase
{
    private EntityManager $entityManager;
    private PeopleRepository $peopleRepository;

    public function setUp(): void
    {
        $kernel = self::bootKernel();

        $this->entityManager = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();

        $this->peopleRepository = $this->entityManager
            ->getRepository(People::class)
        ;
    }

    /**
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function testFindIdIntegracao()
    {
        $people = $this->peopleRepository->findByIntegrationId(100);

        $this->assertIsInt($people->getId());
    }

    /**
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function testInsertFromXml()
    {
        $people = new People();
        $people->setName('Bla');
        $people->setIntegrationId('101');
        
        $this->peopleRepository->insertFromXml($people);

        $peopleNew = $this->peopleRepository->findByIntegrationId(101);

        $this->assertIsInt($peopleNew->getId());
    }
}
