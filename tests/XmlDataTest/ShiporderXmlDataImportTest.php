<?php

namespace App\Tests\XmlDataTest;

use App\Entity\People;
use App\Entity\Shiporder;
use Doctrine\ORM\EntityManager;
use App\XmlData\XmlDataFactory;
use App\Repository\ShiporderRepository;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class ShiporderXmlDataImportTest extends KernelTestCase
{
    private EntityManager $entityManager;
    private ShiporderRepository $shiporderRepository;

    public function setUp(): void
    {
        $kernel = self::bootKernel();

        $this->entityManager = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();

        $this->shiporderRepository = $this->entityManager
            ->getRepository(Shiporder::class)
        ;
    }

    public function testImport()
    {
        $xml = $this->getXml();
        $xmlDataImportFactory = XmlDataFactory::create($xml, $this->entityManager);
        $xmlDataImportFactory->import();

        $this->assertNotEmpty($this->shiporderRepository->findAll());
    }

    /**
     * @return \SimpleXMLElement
     */
    public function getXml(): \SimpleXMLElement
    {
        return simplexml_load_file('tests/XmlDataTest/xml/shiporders.xml');
    }
}