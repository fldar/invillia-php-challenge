<?php

namespace App\Tests\XmlDataTest;

use App\Entity\People;
use App\XmlData\PeopleXmlDataImport;
use App\XmlData\XmlDataFactory;
use Doctrine\ORM\EntityManager;
use App\Repository\PeopleRepository;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class PeopleXmlDataImportTest extends KernelTestCase
{
    private EntityManager $entityManager;
    private PeopleRepository $peopleRepository;

    public function setUp(): void
    {
        $kernel = self::bootKernel();

        $this->entityManager = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();

        $this->peopleRepository = $this->entityManager
            ->getRepository(People::class)
        ;
    }

    public function testImport()
    {
        $xml = $this->getXml();
        $xmlDataImportFactory = XmlDataFactory::create($xml, $this->entityManager);
        $xmlDataImportFactory->import();

        $this->assertNotNull($this->peopleRepository->findByIntegrationId(201));
    }

    /**
     * @return \SimpleXMLElement
     */
    public function getXml(): \SimpleXMLElement
    {
        return simplexml_load_file('tests/XmlDataTest/xml/people.xml');
    }
}